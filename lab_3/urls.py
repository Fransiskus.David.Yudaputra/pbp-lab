from django.urls import path, re_path, include
from .views import friend_list, add_fren

urlpatterns = [
        path('',friend_list,name='friend'),
        path('add', add_fren, name='form'),
]