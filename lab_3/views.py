from django.shortcuts import redirect, render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def friend_list(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
    
@login_required(login_url='/admin/login/')
def add_fren(request):
    context = {}
    form = FriendForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect("/lab-3")
    context['FriendForm']=form
    return render(request, "lab3_form.html",context)