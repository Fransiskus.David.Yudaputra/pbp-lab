from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note
# Create your views here.

def index(request):
    notes = Note.objects.all().values()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'index_lab2.html', response)

def inxml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def injson(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
