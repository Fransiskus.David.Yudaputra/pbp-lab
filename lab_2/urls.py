from django.urls import path, re_path, include
from .views import index,inxml,injson

urlpatterns = [
    path('', index, name='index'),
    path('xml/',inxml,name='inxml' ),
    path('json/',injson,name='injson' ),
    
    
    # TODO Add friends path using friend_list Views
]