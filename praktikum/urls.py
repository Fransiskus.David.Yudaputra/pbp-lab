"""pbp2021 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django import urls
from django.urls import path,include, re_path
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import lab_1.urls as lab_1
from lab_1.views import index as index_lab1
from lab_1.views import friend_list as friend_list_lab1
from lab_2.views import index as index_lab2
from lab_3.views import friend_list as friendlistlab3

urlpatterns = [ 
    path('admin/', admin.site.urls),
    path('lab-1/', include('lab_1.urls')),
    path('lab-2/', include('lab_2.urls')),
    path('lab-3/', include('lab_3.urls')),
    path('lab-4/', include('lab_4.urls')),
    #re_path(r'^$', index_lab1, name='index'),
    re_path(r'^$', friend_list_lab1, name='friend'),
    re_path(r'^$', index_lab2, name='indexlab2'),
    re_path(r'^$', friend_list_lab1, name='lab3fren'),

] + staticfiles_urlpatterns()