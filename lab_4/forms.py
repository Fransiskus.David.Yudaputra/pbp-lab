from django import forms
from django.forms import fields
from lab_2.models import Note as pesan 

class PesanForm(forms.ModelForm):
    class Meta:
        model = pesan
        fields = ['To','From','Title','Message']

form = PesanForm()
	