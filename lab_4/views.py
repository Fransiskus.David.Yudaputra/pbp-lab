from django.shortcuts import render,redirect
from lab_2.models import Note
from .forms import PesanForm
from django.contrib.auth.decorators import login_required

def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

   
@login_required(login_url='/admin/login/')
def add_psan(request):
    context = {}
    form = PesanForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect("/lab-4")
    context['PesanForm']=form
    return render(request, "lab4_form.html",context)

def note_list(request):
    notes = Note.objects.all().values() 
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)