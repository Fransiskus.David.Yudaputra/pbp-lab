from django.urls import path, re_path, include
from .views import index, add_psan, note_list

urlpatterns = [
        path('', index, name='index'),
        path('pesan',add_psan, name='form'),
        path('kartu', note_list, name='note'),

]