/// Flutter code sample for TextButton
/// Referensi Textbutton : https://api.flutter.dev/flutter/material/TextButton-class.html
/// Referensi Statefulwidget : https://docs.flutter.dev/cookbook/forms/validation
// This sample shows how to render a disabled TextButton, an enabled TextButton
// and lastly a TextButton with gradient background.

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Quiz Eduspace';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0x00152238),
          title: const Text(_title)),
        backgroundColor: const Color(0x00152238),
        body: const MyStatelessWidget(),
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFCF6F5FF),
                          Color(0xFCF6F5FF),
                          Color(0xFCF6F5FF),
                        ],
                      ),
                    ),
                  ),
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    padding: const EdgeInsets.all(16.0),
                    primary: Colors.black,
                    textStyle: const TextStyle(fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ButtonPressed()));
                  },
                  child: const Text('Motion : 1-Design - 1 '),
                ),
              ],
            ),
          ),
          const SizedBox(height: 30),
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFCF6F5FF),
                          Color(0xFCF6F5FF),
                          Color(0xFCF6F5FF),
                        ],
                      ),
                    ),
                  ),
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    padding: const EdgeInsets.all(16.0),
                    primary: Colors.black,
                    textStyle: const TextStyle(fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const SoalIsian()));
                  },
                  child: const Text('Motion : 2-Proportion - 2'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
class ButtonPressed extends StatefulWidget {
  @override
  _ButtonPressedState createState() => _ButtonPressedState();
}

class _ButtonPressedState extends State<ButtonPressed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      backgroundColor: const Color(0x00152238),
      title: const Text('Kuis dimulai'),
      ),
      backgroundColor: const Color(0x00152238),
      body: const Center(
        child: Text(
          'Ini adalah tampilan soalnya.',
            style: TextStyle(
            color: Color(0xFCF6F5FF),
            fontSize: 25,
            ),
        ),
      ),
    );
  }
 }
  
  // New button
 class ValidasiJawaban extends StatefulWidget {
  @override
  _ValidasiJawaban createState() => _ValidasiJawaban();
}

class _ValidasiJawaban extends State<ValidasiJawaban> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      backgroundColor: const Color(0x00152238),
      title: const Text('Oke'),
      ),
      backgroundColor: const Color(0x00152238),
      body: const Balik(),
    );
  }
 }

class Balik extends StatelessWidget {
  const Balik({Key? key}) : super(key: key); 
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Text(
          'Jawaban terkirim.',
            style: TextStyle(
            color: Color(0xFCF6F5FF),
            fontSize: 25,
            ),
        ),
        const SizedBox(height: 30),
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFCF6F5FF),
                          Color(0xFCF6F5FF),
                          Color(0xFCF6F5FF),
                        ],
                      ),
                    ),
                  ),
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    padding: const EdgeInsets.all(16.0),
                    primary: Colors.black,
                    textStyle: const TextStyle(fontSize: 20),
                  ),
                  onPressed: () {
                    // Karena sebelumnya saya memakai navigator push, saya ingin mengganti dengan pop
                    // Referensi https://newbedev.com/flutter-how-do-i-pop-two-screens-without-using-named-routing
                    int count = 0;
                    Navigator.of(context).popUntil((_) => count++ >= 2); 
                  },
                  child: const Text('Back to awal?'),
                ),
              ],
            ),
          ), 
        ],
      ),
    );
  }
}

 class SoalIsian extends StatefulWidget {
  const SoalIsian({Key? key}) : super(key: key);
  @override
  _SoalIsian createState() {
    return _SoalIsian(); 
  }
}

class _SoalIsian extends State<SoalIsian> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: _formKey,
      home: Scaffold(
      appBar: AppBar(
      backgroundColor: const Color(0x00152238),
      title: const Text('Kuis dimulai'),
      ),
      backgroundColor: const Color(0x00152238),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            '1 + 1 = ?',
            style: TextStyle(
            color: Color(0xFCF6F5FF),
            fontSize: 25,
            ),   
          ),
          TextFormField(
            style: const TextStyle(color: Colors.white),
            // The validator receives the text that the user has entered.
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Masukkan jawaban';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ValidasiJawaban()));
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
      ),
    );
  }
 }